import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FacebookModule } from 'ngx-facebook';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';

export const firebaseConfig = {
  apiKey: "AIzaSyAPvDEdOoGeldbbmVWShXguFmWFaX79DI8",
  authDomain: "genesismarkets-a31ff.firebaseapp.com",
  databaseURL: "https://genesismarkets-a31ff.firebaseio.com",
  projectId: "genesismarkets-a31ff",
  storageBucket: "genesismarkets-a31ff.appspot.com",
  messagingSenderId: "10702823051"
};

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FacebookModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule
  ],
  providers: [
    // AngularFireDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
