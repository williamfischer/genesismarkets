import { Component } from '@angular/core';
import { FacebookService, InitParams, LoginResponse } from 'ngx-facebook';

import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', './materialize.min.css']
})

export class AppComponent{

  myDate: any;
  myimg: string;

  constructor(private fb: FacebookService, private db: AngularFireDatabase) {
    let initParams: InitParams = {
      appId: '1684621208533373',
      xfbml: true,
      version: 'v2.8'
    };

    setInterval(() => {
      this.myDate = new Date();
    }, 1000);

    fb.init(initParams);
  }

  loginWithFacebook(): void {

    this.fb.login()
      .then((response: LoginResponse) => {
        this.db.list('Users/' + response.authResponse.userID + '/Inventory').subscribe(actions => {

          for (let entry of actions) {
              this.myimg = "assets/drifterskins/" + entry.Owner.CID + "/" + entry.SKID + "/icon.png";
          }

        });
      })
      .catch((error: any) => console.error(error));

  }

}
